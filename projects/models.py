from django.db import models
from django.conf import settings
from tasks.models import Task
from datetime import datetime


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="projects",
        on_delete=models.CASCADE,
        null=True,
    )

    def add_task(self, name: str, start_date: datetime, due_date: datetime):
        return Task.objects.create(
            name=name,
            start_date=start_date,
            due_date=due_date,
            project=self,
            assignee=self.owner,
        )

    def __str__(self):
        return self.name
