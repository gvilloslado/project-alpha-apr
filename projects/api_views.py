from projects.models import Project
from django.http import HttpRequest, JsonResponse

# import json


def show_projects(request: HttpRequest) -> JsonResponse:
    projects = Project.objects.all()
    projects_list = [
        {
            "id": project.id,
            "description": project.description,
            "owner": project.owner.username,
        }
        for project in projects
    ]
    # projects_json_str = json.dumps(projects_list)
    # return HttpResponse(projects_json_str, content_type="application/json")

    return JsonResponse({"projects": projects_list})
