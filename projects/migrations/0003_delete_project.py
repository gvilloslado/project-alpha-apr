# Generated by Django 5.0.6 on 2024-05-28 18:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("projects", "0002_project_description"),
    ]

    operations = [
        migrations.DeleteModel(
            name="Project",
        ),
    ]
