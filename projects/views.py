from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpRequest, HttpResponse
from django.contrib.auth.decorators import login_required
from projects.models import Project
from projects.forms import ProjectForm


@login_required
def list_projects(request: HttpRequest) -> HttpResponse:
    projects = Project.objects.filter(owner=request.user)
    return render(request, "projects/list.html", {"list_projects": projects})


@login_required
def show_project(request: HttpRequest, id) -> HttpResponse:
    project = get_object_or_404(Project, id=id)
    return render(
        request, "projects/detail.html", {"project_details": project}
    )


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    return render(request, "projects/create.html", {"form": form})
