from django.urls import path
from projects.api_views import show_projects

urlpatterns = [path("projects/", show_projects)]
